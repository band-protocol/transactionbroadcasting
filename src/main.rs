use reqwest::Client;
use serde::{Deserialize, Serialize};
use chrono::Utc;

#[derive(Serialize, Debug)]
struct TransactionPayload {
    symbol: String,
    price: u64,
    timestamp: u64
}

#[derive(Deserialize, Debug)]
struct BroadcastResponse {
    tx_hash: String,
}

struct TransactionClient {
    client: Client,
    broadcast_url: String,
    check_url_template: String
}

#[derive(Deserialize, Debug)]
struct StatusResponse {
    tx_status: String
}

impl TransactionClient {
    pub fn new(broadcast_url: &str, check_url_template: &str) -> TransactionClient {
        TransactionClient {
            client: Client::new(),
            broadcast_url: broadcast_url.to_string(),
            check_url_template: check_url_template.to_string()
        }
    }

    pub async fn broadcast(&self, payload: TransactionPayload) -> Result<String, reqwest::Error> {
        let response = self.client.post(&self.broadcast_url)
            .json(&payload)
            .send()
            .await?;
        println!("Response status: {}", response.status());

        let response_body: BroadcastResponse = response.json().await?;
        Ok(response_body.tx_hash)
    }

    pub async fn monitor_transaction(&self, tx_hash: &str, interval: u64) -> Result<String, reqwest::Error> {
        loop {
            let response = self.client.get(&format!("{}{}", self.check_url_template, tx_hash))
                .send()
                .await?;
            let response_body: StatusResponse = response.json().await?;
            println!("Transaction {} is {}, auto refresh in {} seconds.", tx_hash, response_body.tx_status, interval);
            if response_body.tx_status == "CONFIRMED" || response_body.tx_status == "FAILED" {
                return Ok(tx_hash.to_string());
            }
                
            tokio::time::sleep(tokio::time::Duration::from_secs(interval)).await;
        }
    }
}

#[tokio::main]
async fn main() {
    let broadcast_url = "https://mock-node-wgqbnxruha-as.a.run.app/broadcast";
    let check_url_template = "https://mock-node-wgqbnxruha-as.a.run.app/check/";

    let client = TransactionClient::new(broadcast_url, check_url_template);

    let payload = TransactionPayload {
        symbol: "BTA".to_string(),
        price: 4500, 
        timestamp: Utc::now().timestamp() as u64
    };
    match client.broadcast(payload).await {
        Ok(response) => match client.monitor_transaction(&response, 1).await {
            Ok(tx_hash) => println!("Transaction hash: {}", tx_hash),
            Err(e) => println!("Error: {}", e)
        }
        Err(e) => println!("Error: {}", e)
    }
}
